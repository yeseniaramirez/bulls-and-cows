from random import randint

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    my_list = []
    while len(my_list) < 4:
        n = randint(0,9)
        if n not in my_list:
            my_list.append(n)
           #print(my_list)
    return my_list

def parse_numbers(s):
    l = []
    for n in (s):
        number = int(n)
        l.append(number)
   # print(l)
    return l

def count_exact_matches(first, second):
    count = 0
    for a,b in zip(first, second):
        if a == b:
            count = count + 1 #(or count += 1)
    return count

def count_common_entries(first, second):
    count = 0
    for entry in first:
            if entry in second: 
                count = count + 1
    return count

def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    #print("secret: ", secret)
    number_of_guesses = 20


    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        while len(guess) != 4:
            print("you must enter a four-digit number")
            guess = input(prompt)

        parse_numbers(guess)

        converted_guess = parse_numbers(guess)

        total_exact_matches = count_exact_matches(converted_guess, secret)

        total_common_entries = count_common_entries(converted_guess, secret)
       # print("total common entries: ", total_common_entries)

        if total_exact_matches == 4:
            print("You've guessed the right numbers!!")
            return 

        print("total number of bulls: ", total_exact_matches)

        print("total numbers of cows: ", total_common_entries - total_exact_matches )


        
    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.
    print("the secret was: ", secret)

# Runs the game
def run():
    response = input("Do you want to play a game? Y or N? ")
    
    while response == "Y":
        play_game()
        response = input("Do you want to play a game? Y or N? ")
    # TODO: Delete the word pass, below, and have this function:
    # Call the play_game function
    # Ask if they want to play again
    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    pass


if __name__ == "__main__":
    run()
